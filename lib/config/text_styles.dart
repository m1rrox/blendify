import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class BlendifyTextStyle {
  BlendifyTextStyle._();

  static final TextStyle heading1 = GoogleFonts.montserrat(
    fontSize: 32.0,
    fontWeight: FontWeight.w600,
  );
}
