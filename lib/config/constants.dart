class BlendifyConstants {
  BlendifyConstants._();

  static const String cocktailApiBaseUrl =
      'https://www.thecocktaildb.com/api/json/v1/1';

  static const String randomCocktailEndpointUrl =
      '$cocktailApiBaseUrl/random.php';
}
