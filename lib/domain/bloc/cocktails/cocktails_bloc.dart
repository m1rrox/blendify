import 'dart:isolate';

import 'package:blendify/domain/entity/cocktail.dart';
import 'package:blendify/domain/repository/cocktail_repository.dart';
import 'package:blendify/page/cocktails/widgets/cocktail_card.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'cocktails_event.dart';
part 'cocktails_state.dart';

class CocktailsBloc extends Bloc<CocktailsEvent, CocktailsState> {
  CocktailsBloc({required this.cocktailRepository})
      : super(CocktailsState.empty()) {
    on<LoadRandomCocktails>((event, emit) async {
      emit(state.copyWith(isLoading: true));

      final randomCocktails = await cocktailRepository.getRandomCocktails();

      emit(state.copyWith(isLoading: false, randomCocktails: randomCocktails));
    });

    on<LoadLikedCocktails>((event, emit) async {
      emit(state.copyWith(isLoading: true));

      final cocktails =
          await cocktailRepository.getLikedCocktails();

      emit(state.copyWith(isLoading: false, likedCocktails: cocktails));
    });

    on<LikeCocktailButtonPressed>((event, emit) async {
      emit(state.copyWith(isLoading: true));

      final cocktail =
          await cocktailRepository.addLikedCocktail(event.cocktail);

      emit(state.copyWith(isLoading: false));

      add(const LoadLikedCocktails());
    });

    on<DeleteCocktailButtonPressed>((event, emit) async {
      emit(state.copyWith(isLoading: true));

      final cocktail =
          await cocktailRepository.deleteLikedCocktail(event.cocktailId);

      emit(state.copyWith(isLoading: false));

      add(const LoadLikedCocktails());
    });
  }

  final ICocktailRepository cocktailRepository;
}
