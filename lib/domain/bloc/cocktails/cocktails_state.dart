part of 'cocktails_bloc.dart';

class CocktailsState extends Equatable {
  const CocktailsState({
    required this.randomCocktails,
    required this.likedCocktails,
    required this.isLoading,
  });

  factory CocktailsState.empty() => const CocktailsState(
        likedCocktails: [],
        randomCocktails: [],
        isLoading: false,
      );

  CocktailsState copyWith({
    List<Cocktail>? randomCocktails,
    List<Cocktail>? likedCocktails,
    bool? isLoading,
  }) =>
      CocktailsState(
        isLoading: isLoading ?? this.isLoading,
        likedCocktails: likedCocktails ?? this.likedCocktails,
        randomCocktails: randomCocktails ?? this.randomCocktails,
      );

  final List<Cocktail> randomCocktails;
  final List<Cocktail> likedCocktails;
  final bool isLoading;

  @override
  List<Object> get props => [likedCocktails, randomCocktails, isLoading];
}
