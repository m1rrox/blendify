part of 'cocktails_bloc.dart';

abstract class CocktailsEvent extends Equatable {
  const CocktailsEvent();

  @override
  List<Object> get props => [];
}

class LoadRandomCocktails extends CocktailsEvent {
  const LoadRandomCocktails();
}

class LoadLikedCocktails extends CocktailsEvent {
  const LoadLikedCocktails();
}

class LikeCocktailButtonPressed extends CocktailsEvent {
  const LikeCocktailButtonPressed({required this.cocktail});

  final Cocktail cocktail;

  @override
  List<Object> get props => [cocktail];
}

class DeleteCocktailButtonPressed extends CocktailsEvent {
  const DeleteCocktailButtonPressed({required this.cocktailId});

  final String cocktailId;

  @override
  List<Object> get props => [cocktailId];
}
