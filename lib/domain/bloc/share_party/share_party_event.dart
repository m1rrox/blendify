part of 'share_party_bloc.dart';

abstract class SharePartyEvent extends Equatable {
  const SharePartyEvent();

  @override
  List<Object> get props => [];
}

class InstagramShareButtonPressed extends SharePartyEvent {
  const InstagramShareButtonPressed({required this.party});

  final Party party;

  @override
  List<Object> get props => [party];
}
