import 'dart:io';
import 'dart:typed_data';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:screenshot/screenshot.dart';
import 'package:social_share/social_share.dart';
import 'package:path_provider/path_provider.dart';
import 'package:blendify/domain/entity/party.dart';
import 'package:blendify/page/create_party/widgets/share_party_card.dart';

part 'share_party_event.dart';
part 'share_party_state.dart';

class SharePartyBloc extends Bloc<SharePartyEvent, SharePartyState> {
  SharePartyBloc() : super(SharePartyInitial()) {
    on<InstagramShareButtonPressed>((event, emit) async {
      final directory = await getTemporaryDirectory();
      final imagePath = '${directory.path}/instashare.png';

      final Uint8List imageBytes =
          await _screenshotController.captureFromWidget(
        SharePartyCard(party: event.party),
      );

      File image = await File(imagePath).create();
      await image.writeAsBytes(imageBytes);

      await SocialShare.shareInstagramStory(
        image.path,
        backgroundTopColor: "ffe4e1",
        backgroundBottomColor: "ffe4e1",
      );
    });
  }

  final SocialShare _socialShare = SocialShare();
  final ScreenshotController _screenshotController = ScreenshotController();
}
