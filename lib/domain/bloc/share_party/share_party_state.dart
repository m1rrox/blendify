part of 'share_party_bloc.dart';

abstract class SharePartyState extends Equatable {
  const SharePartyState();
  
  @override
  List<Object> get props => [];
}

class SharePartyInitial extends SharePartyState {}
