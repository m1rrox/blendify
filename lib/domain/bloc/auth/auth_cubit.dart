import '../../entity/profile.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:firebase_auth/firebase_auth.dart';

part 'auth_state.dart';

class AuthCubit extends Cubit<AuthState> {
  AuthCubit({required this.firebaseAuth}) : super(const Unauthenticated()) {
    emit(const Unauthenticated());

    firebaseAuth.authStateChanges().listen((user) {
      user == null
          ? emit(const Unauthenticated())
          : emit(Authenticated(
              profile: Profile(
              uid: user.uid,
              name: user.displayName,
              avatarUrl: user.photoURL,
            )));
    });
  }

  final FirebaseAuth firebaseAuth;
}
