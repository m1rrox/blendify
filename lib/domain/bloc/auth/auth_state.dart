part of 'auth_cubit.dart';

abstract class AuthState extends Equatable {
  const AuthState();

  @override
  List<Object> get props => [];
}

class Authenticated extends AuthState {
  const Authenticated({required this.profile});

  final Profile profile;

  @override
  List<Object> get props => [profile];
}

class Unauthenticated extends AuthState {
  const Unauthenticated();
}
