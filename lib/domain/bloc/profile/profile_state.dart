part of 'profile_bloc.dart';

class ProfileState extends Equatable {
  const ProfileState({
    required this.isLoading,
    required this.profile,
    required this.parties,
  });

  factory ProfileState.empty() => ProfileState(
        isLoading: false,
        profile: Profile.empty(),
        parties: const [],
      );

  final bool isLoading;
  final Profile profile;
  final List<Party> parties;

  ProfileState copyWith({
    bool? isLoading,
    Profile? profile,
    List<Party>? parties,
  }) {
    return ProfileState(
      isLoading: isLoading ?? this.isLoading,
      profile: profile ?? this.profile,
      parties: parties ?? this.parties,
    );
  }

  @override
  List<Object> get props => [isLoading, profile, parties];
}
