part of 'profile_bloc.dart';

abstract class ProfileEvent extends Equatable {
  const ProfileEvent();

  @override
  List<Object> get props => [];
}

class UserLoggedIn extends ProfileEvent {
  const UserLoggedIn({required this.profile});

  final Profile profile;

  @override
  List<Object> get props => [profile];
}

class LoadCreatedParties extends ProfileEvent {
  const LoadCreatedParties();
}
