import 'package:blendify/domain/entity/party.dart';
import 'package:blendify/domain/repository/party_repository.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../../repository/profile_repository.dart';
import '../../entity/profile.dart';

part 'profile_event.dart';
part 'profile_state.dart';

class ProfileBloc extends Bloc<ProfileEvent, ProfileState> {
  ProfileBloc({
    required this.profileRepository,
    required this.partyRepository,
  }) : super(ProfileState.empty()) {
    on<UserLoggedIn>((event, emit) async {
      emit(state.copyWith(isLoading: true));

      await profileRepository.createProfile(event.profile);
      final profile = await profileRepository.getProfileById(event.profile.uid);

      if (profile != null) emit(state.copyWith(profile: profile));

      emit(state.copyWith(isLoading: false));
    });

    on<LoadCreatedParties>((event, emit) async {
      emit(state.copyWith(isLoading: true));

      final userParties = await partyRepository.getCreatedParties();

      emit(state.copyWith(parties: userParties, isLoading: false));
    });
  }

  final IProfileRepository profileRepository;
  final IPartyRepository partyRepository;
}
