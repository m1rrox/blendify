part of 'create_party_bloc.dart';

class CreatePartyState extends Equatable {
  const CreatePartyState({
    required this.avatarUrl,
    required this.location,
    required this.isLoading,
    required this.isFinished,
  });

  factory CreatePartyState.empty() => const CreatePartyState(
        avatarUrl: '',
        location: '',
        isLoading: false,
        isFinished: false,
      );

  CreatePartyState copyWith({
    String? avatarUrl,
    String? location,
    bool? isLoading,
    bool? isFinished,
  }) =>
      CreatePartyState(
        avatarUrl: avatarUrl ?? this.avatarUrl,
        location: location ?? this.location,
        isLoading: isLoading ?? this.isLoading,
        isFinished: isFinished ?? this.isFinished,
      );

  final String avatarUrl;
  final String location;
  final bool isLoading;
  final bool isFinished;

  @override
  List<Object> get props => [avatarUrl, location, isLoading, isFinished];
}
