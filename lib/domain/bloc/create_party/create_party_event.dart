part of 'create_party_bloc.dart';

abstract class CreatePartyEvent extends Equatable {
  const CreatePartyEvent();

  @override
  List<Object> get props => [];
}

class AvatarButtonPressed extends CreatePartyEvent {
  const AvatarButtonPressed();
}

class LocationRetrieved extends CreatePartyEvent {
  const LocationRetrieved({required this.position});

  final Position position;

  @override
  List<Object> get props => [position];
}

class FinishButtonPressed extends CreatePartyEvent {
  const FinishButtonPressed({required this.party});

  final Party party;

  @override
  List<Object> get props => [party];
}
