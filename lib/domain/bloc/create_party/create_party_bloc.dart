import 'dart:io';

import 'package:blendify/domain/entity/party.dart';
import 'package:blendify/domain/entity/position.dart';
import 'package:blendify/domain/repository/party_repository.dart';
import 'package:blendify/util/location_manager.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:image_picker/image_picker.dart';

part 'create_party_event.dart';
part 'create_party_state.dart';

class CreatePartyBloc extends Bloc<CreatePartyEvent, CreatePartyState> {
  CreatePartyBloc({
    required this.currentUserId,
    required this.partyRepository,
    required this.locationManager,
    required this.storage,
    required this.imagePicker,
  }) : super(CreatePartyState.empty()) {
    on<AvatarButtonPressed>((event, emit) async {
      emit(state.copyWith(isLoading: true));

      XFile? image = await imagePicker.pickImage(source: ImageSource.gallery);

      if (image != null) {
        String reference = '/$currentUserId/${image.name}';
        await storage.ref(reference).putFile(File(image.path));
        final imageUrl = await storage.ref(reference).getDownloadURL();
        emit(state.copyWith(avatarUrl: imageUrl));
      }

      emit(state.copyWith(isLoading: false));
    });

    on<LocationRetrieved>((event, emit) async {
      emit(state.copyWith(isLoading: true));

      final address =
          await locationManager.getAddressFromPosition(event.position);

      emit(state.copyWith(location: address, isLoading: false));
    });

    on<FinishButtonPressed>((event, emit) async {
      emit(state.copyWith(isLoading: true));

      final party = await partyRepository.createParty(event.party);

      emit(state.copyWith(isLoading: false, isFinished: true));

      emit(state.copyWith(avatarUrl: '', location: '', isFinished: false));
    });
  }

  final IPartyRepository partyRepository;
  final LocationManager locationManager;
  final ImagePicker imagePicker;
  final FirebaseStorage storage;
  final String currentUserId;
}
