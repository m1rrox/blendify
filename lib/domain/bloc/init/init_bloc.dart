import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:firebase_core/firebase_core.dart';

import '../../../firebase_options.dart';

part 'init_event.dart';
part 'init_state.dart';

class InitBloc extends Bloc<InitEvent, InitState> {
  InitBloc() : super(const AppInitInProgress()) {
    on<AppStarted>((event, emit) => _appStarted(event, emit));
  }

  void _appStarted(AppStarted event, Emitter<InitState> emit) async {
    emit(const AppInitInProgress());

    await Firebase.initializeApp(
      options: DefaultFirebaseOptions.currentPlatform,
    );

    await Future.delayed(
      const Duration(seconds: 3),
    );

    emit(const AppInitSuccess());
  }
}
