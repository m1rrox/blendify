import 'package:equatable/equatable.dart';
import 'package:uuid/uuid.dart';

enum PartyType { private, public }

class Party extends Equatable {
  Party({
    String? id,
    required this.name,
    required this.description,
    required this.imageUrl,
    required this.startAt,
    required this.location,
    required this.type,
  }) : id = id ?? const Uuid().v4();

  Party.empty()
      : id = const Uuid().v4(),
        name = '',
        description = '',
        startAt = DateTime.now().add(
          const Duration(minutes: 30),
        ),
        imageUrl = '',
        location = '',
        type = null;

  Party copyWith({
    String? name,
    String? description,
    DateTime? startAt,
    String? location,
    String? imageUrl,
    PartyType? type,
  }) {
    return Party(
      id: id,
      name: name ?? this.name,
      description: description ?? this.description,
      startAt: startAt ?? this.startAt,
      location: location ?? this.location,
      imageUrl: imageUrl ?? this.imageUrl,
      type: type ?? this.type,
    );
  }

  final String id;
  final String name;
  final String description;
  final DateTime startAt;
  final String imageUrl;
  final String location;
  final PartyType? type;

  @override
  List<Object?> get props => [
        id,
        name,
        description,
        startAt,
        location,
        imageUrl,
        type,
      ];
}
