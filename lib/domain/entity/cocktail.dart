import 'package:equatable/equatable.dart';

class Cocktail extends Equatable {
  const Cocktail({
    required this.id,
    required this.title,
    required this.glass,
    required this.instructions,
    required this.imageUrl,
  });

  final String id;
  final String title;
  final String glass;
  final String instructions;
  final String imageUrl;

  @override
  List<Object?> get props => [id, title, glass, instructions, imageUrl];
}
