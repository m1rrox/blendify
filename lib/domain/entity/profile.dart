import 'package:equatable/equatable.dart';

class Profile extends Equatable {
  const Profile({
    required this.uid,
    this.name,
    this.avatarUrl,
  });

  factory Profile.empty() => const Profile(
        uid: '',
        name: '',
        avatarUrl: '',
      );

  final String uid;
  final String? name;
  final String? avatarUrl;

  @override
  List<Object?> get props => [uid, name, avatarUrl];
}
