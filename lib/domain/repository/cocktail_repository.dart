import 'package:blendify/domain/entity/cocktail.dart';

abstract class ICocktailRepository {
  Future<List<Cocktail>> getRandomCocktails();

  Future<Cocktail> addLikedCocktail(Cocktail cocktail);

  Future<void> deleteLikedCocktail(String cocktailId);

  Future<List<Cocktail>> getLikedCocktails();
}
