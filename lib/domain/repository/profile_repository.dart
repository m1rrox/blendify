import '../entity/profile.dart';

abstract class IProfileRepository {
  Future<Profile> createProfile(Profile profile);

  Future<Profile?> getProfileById(String id);
}
