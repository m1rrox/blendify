import 'package:blendify/domain/entity/party.dart';

abstract class IPartyRepository {
  Future<Party> createParty(Party party);

  Future<List<Party>> getCreatedParties();
}
