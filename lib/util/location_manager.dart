import 'package:blendify/domain/entity/position.dart';
import 'package:geocoding/geocoding.dart';

class LocationManager {
  const LocationManager();

  Future<String> getAddressFromPosition(Position position) async {
    List<Placemark> placemarks =
        await placemarkFromCoordinates(position.latitude, position.longitude);
    final placemark = placemarks[0];

    return '${placemark.street}, ${placemark.locality}, ${placemark.country}';
  }
}
