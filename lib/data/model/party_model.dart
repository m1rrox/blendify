import 'package:blendify/domain/entity/party.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class PartyModel {
  const PartyModel({
    required this.id,
    required this.name,
    required this.description,
    required this.imageUrl,
    required this.startAt,
    required this.location,
    required this.type,
  });

  final String id;
  final String name;
  final String description;
  final DateTime startAt;
  final String imageUrl;
  final String location;
  final String type;

  factory PartyModel.fromDocument(
    DocumentSnapshot<Map<String, dynamic>> document,
    SnapshotOptions? options,
  ) {
    final data = document.data()!;
    return PartyModel(
      id: data['id'],
      name: data['name'],
      description: data['description'],
      imageUrl: data['imageUrl'],
      startAt: (data['startAt'] as Timestamp).toDate(),
      location: data['location'],
      type: data['type'],
    );
  }

  factory PartyModel.fromEntity(Party entity) => PartyModel(
        id: entity.id,
        name: entity.name,
        description: entity.description,
        imageUrl: entity.imageUrl,
        startAt: entity.startAt,
        location: entity.location,
        type: entity.type?.name as String,
      );

  Map<String, dynamic> toDocument() => {
        'id': id,
        'name': name,
        'description': description,
        'imageUrl': imageUrl,
        'startAt': startAt,
        'location': location,
        'type': type,
      };

  Party toEntity() => Party(
        id: id,
        name: name,
        description: description,
        startAt: startAt,
        location: location,
        imageUrl: imageUrl,
        type: PartyType.values.byName(type),
      );
}
