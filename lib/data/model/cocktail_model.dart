import 'package:blendify/domain/entity/cocktail.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class CocktailModel {
  const CocktailModel({
    required this.id,
    required this.title,
    required this.glass,
    required this.instructions,
    required this.imageUrl,
  });

  final String id;
  final String title;
  final String glass;
  final String instructions;
  final String imageUrl;

  factory CocktailModel.fromDocument(
    DocumentSnapshot<Map<String, dynamic>> document,
    SnapshotOptions? options,
  ) {
    final data = document.data()!;
    return CocktailModel(
      id: document.id,
      title: data['title'],
      glass: data['glass'],
      instructions: data['instructions'],
      imageUrl: data['imageUrl'],
    );
  }

  factory CocktailModel.fromEntity(Cocktail entity) => CocktailModel(
        id: entity.id,
        title: entity.title,
        glass: entity.glass,
        instructions: entity.instructions,
        imageUrl: entity.imageUrl,
      );

  CocktailModel.fromJson(Map<String, dynamic> json)
      : id = json['idDrink'],
        title = json['strDrink'],
        glass = json['strGlass'],
        instructions = json['strInstructions'],
        imageUrl = json['strDrinkThumb'];

  Map<String, dynamic> toDocument() => {
        'title': title,
        'glass': glass,
        'instructions': instructions,
        'imageUrl': imageUrl,
      };

  Cocktail toEntity() => Cocktail(
        id: id,
        title: title,
        glass: glass,
        instructions: instructions,
        imageUrl: imageUrl,
      );
}
