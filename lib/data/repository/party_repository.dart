import 'package:blendify/data/model/party_model.dart';
import 'package:blendify/domain/entity/party.dart';
import 'package:blendify/domain/repository/party_repository.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class PartyRepository extends IPartyRepository {
  PartyRepository({
    required this.currentUserId,
    required this.firestore,
  });

  late CollectionReference<PartyModel> parties = firestore
      .collection('users')
      .doc(currentUserId)
      .collection('parties')
      .withConverter(
        fromFirestore: (document, options) =>
            PartyModel.fromDocument(document, options),
        toFirestore: (model, options) => model.toDocument(),
      );

  final String currentUserId;
  final FirebaseFirestore firestore;

  @override
  Future<Party> createParty(Party party) async {
    await parties.doc(party.id).set(PartyModel.fromEntity(party));

    return party;
  }

  @override
  Future<List<Party>> getCreatedParties() async {
    final querySnapshot = await parties.get();

    return querySnapshot.docs
        .map((document) => document.data().toEntity())
        .toList();
  }
}
