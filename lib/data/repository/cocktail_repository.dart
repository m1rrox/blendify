import 'package:blendify/config/constants.dart';
import 'package:blendify/data/model/cocktail_model.dart';
import 'package:blendify/domain/entity/cocktail.dart';
import 'package:blendify/domain/repository/cocktail_repository.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dio/dio.dart';

class CocktailRepository extends ICocktailRepository {
  CocktailRepository({
    required this.currentUserId,
    required this.firestore,
    required this.networkClient,
  });

  final String currentUserId;
  final FirebaseFirestore firestore;
  final Dio networkClient;

  late CollectionReference<CocktailModel> likedCocktails = firestore
      .collection('users')
      .doc(currentUserId)
      .collection('likedCocktails')
      .withConverter(
        fromFirestore: (document, options) =>
            CocktailModel.fromDocument(document, options),
        toFirestore: (model, options) => model.toDocument(),
      );

  @override
  Future<Cocktail> addLikedCocktail(Cocktail cocktail) async {
    await likedCocktails
        .doc(cocktail.id)
        .set(CocktailModel.fromEntity(cocktail));

    return cocktail;
  }

  @override
  Future<List<Cocktail>> getRandomCocktails() async {
    List<Cocktail> cocktails = [];

    for (var cocktailIndex = 0; cocktailIndex < 10; cocktailIndex++) {
      final response =
          await networkClient.get(BlendifyConstants.randomCocktailEndpointUrl);
      final randomCocktail = CocktailModel.fromJson(
        response.data['drinks'][0],
      ).toEntity();
      cocktails.add(randomCocktail);
    }

    return cocktails;
  }

  @override
  Future<List<Cocktail>> getLikedCocktails() async {
    final querySnapshot = await likedCocktails.get();

    return querySnapshot.docs
        .map((document) => document.data().toEntity())
        .toList();
  }

  @override
  Future<void> deleteLikedCocktail(String cocktailId) async {
    await likedCocktails.doc(cocktailId).delete();
  }
}
