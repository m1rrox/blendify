import 'package:blendify/domain/bloc/share_party/share_party_bloc.dart';
import 'package:blendify/domain/entity/party.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class PartyCard extends StatelessWidget {
  const PartyCard({Key? key, required this.party}) : super(key: key);

  final Party party;

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 1.5,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12.0),
      ),
      color: Colors.blue.shade50,
      shadowColor: Colors.black26,
      child: Container(
        height: 80.0,
        padding: const EdgeInsets.all(4.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Container(
              width: 72.0,
              decoration: BoxDecoration(
                color: Colors.blue.shade400,
                borderRadius: const BorderRadius.horizontal(
                  left: Radius.circular(12.0),
                ),
                image: DecorationImage(
                  fit: BoxFit.cover,
                  image: NetworkImage(party.imageUrl),
                ),
              ),
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Text(
                    party.name,
                    textAlign: TextAlign.center,
                  ),
                  Text(
                    party.startAt.toUtc().toString(),
                    textAlign: TextAlign.center,
                  ),
                  Text(
                    party.location,
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
            ),
            InkWell(
              splashColor: Colors.white30,
              onTap: () => context
                  .read<SharePartyBloc>()
                  .add(InstagramShareButtonPressed(party: party)),
              child: Container(
                width: 42.0,
                alignment: Alignment.center,
                decoration: const BoxDecoration(
                  color: Colors.blue,
                  borderRadius: BorderRadius.horizontal(
                    right: Radius.circular(12.0),
                  ),
                ),
                child: const Icon(
                  Icons.ios_share,
                  color: Colors.white,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
