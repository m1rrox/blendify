import 'dart:math';

import 'package:blendify/page/widgets/loader.dart';
import 'package:blendify/page/widgets/party_card.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutterfire_ui/auth.dart';

import '../domain/bloc/profile/profile_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ProfilePage extends StatelessWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder<ProfileBloc, ProfileState>(
        builder: (context, state) {
          return Stack(
            children: <Widget>[
              CustomScrollView(
                physics: const BouncingScrollPhysics(),
                slivers: <Widget>[
                  SliverPersistentHeader(
                    delegate: _ProfilePageHeaderDelegate(
                      name: state.profile.name!,
                      imageUrl: state.profile.avatarUrl!,
                    ),
                  ),
                  SliverToBoxAdapter(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          const SizedBox(height: 16.0),
                          SignOutButton(
                            auth: context.read<FirebaseAuth>(),
                          ),
                          const SizedBox(height: 16.0),
                          const Text(
                            'Your parties',
                            textAlign: TextAlign.center,
                          ),
                          ...state.parties
                              .map((party) => PartyCard(party: party))
                              .toList(),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              if (state.isLoading) const Loader(),
            ],
          );
        },
      ),
    );
  }
}

class _ProfilePageHeaderDelegate extends SliverPersistentHeaderDelegate {
  const _ProfilePageHeaderDelegate(
      {required this.name, required this.imageUrl});

  final String name;
  final String imageUrl;

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return Stack(
      fit: StackFit.expand,
      children: <Widget>[
        Positioned.fill(
          child: DecoratedBox(
            decoration: BoxDecoration(
              color: Colors.blue.shade100,
              borderRadius: const BorderRadius.vertical(
                bottom: Radius.circular(16.0),
              ),
            ),
          ),
        ),
        Positioned.fill(
          child: Center(
            child: SizedBox(
              child: Flex(
                mainAxisAlignment: MainAxisAlignment.center,
                direction: flexAxis(shrinkOffset),
                children: <Widget>[
                  CircleAvatar(
                    backgroundColor: Colors.blue.shade300,
                    foregroundImage: NetworkImage(imageUrl),
                    radius: avatarRadius(shrinkOffset),
                  ),
                  const SizedBox(
                    height: 20.0,
                    width: 20.0,
                  ),
                  Text(
                    name,
                    style: TextStyle(
                      fontSize: nameFontSize(shrinkOffset),
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }

  double avatarRadius(double shrinkOffset) {
    return 50.0 - (max(0, shrinkOffset) / maxExtent * 25.0);
  }

  double nameFontSize(double shrinkOffset) {
    return 32.0 - (max(0, shrinkOffset) / maxExtent * 10.0);
  }

  Axis flexAxis(double shrinkOffset) {
    return shrinkOffset > (maxExtent / 2.5) ? Axis.horizontal : Axis.vertical;
  }

  @override
  double get maxExtent => 320.0;

  @override
  double get minExtent => 64.0;

  @override
  bool shouldRebuild(covariant SliverPersistentHeaderDelegate oldDelegate) =>
      true;
}
