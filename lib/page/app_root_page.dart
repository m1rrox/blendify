import 'package:blendify/domain/bloc/create_party/create_party_bloc.dart';
import 'package:blendify/domain/bloc/profile/profile_bloc.dart';
import 'package:blendify/domain/bloc/share_party/share_party_bloc.dart';
import 'package:blendify/domain/entity/profile.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rxdart/subjects.dart';

import 'cocktails/cocktails_page.dart';
import 'create_party/create_party_page.dart';
import 'profile_page.dart';

class AppRootPage extends StatefulWidget {
  const AppRootPage({Key? key}) : super(key: key);

  @override
  State<AppRootPage> createState() => _AppRootPageState();
}

class _AppRootPageState extends State<AppRootPage> {
  final int initialPageIndex = 0;

  late PageController _pageController;
  late BehaviorSubject<int> _selectedPage;
  late String? avatarUrl;

  @override
  void initState() {
    super.initState();
    _pageController = PageController(initialPage: initialPageIndex);
    _selectedPage = BehaviorSubject<int>.seeded(initialPageIndex);
  }

  @override
  void dispose() {
    _selectedPage.close();
    _pageController.dispose();
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    final profile = context.read<Profile>();

    context.read<ProfileBloc>()
      ..add(UserLoggedIn(profile: profile))
      ..add(const LoadCreatedParties());

    avatarUrl = profile.avatarUrl;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView(
        controller: _pageController,
        physics: const BouncingScrollPhysics(),
        onPageChanged: (selectedPage) => _selectedPage.add(selectedPage),
        children: <Widget>[
          const CocktailsPage(),
          BlocBuilder<CreatePartyBloc, CreatePartyState>(
            buildWhen: (previous, current) => current.isFinished,
            builder: (context, state) {
              return CreatePartyPage(
                key: UniqueKey(),
              );
            },
          ),
          BlocProvider<SharePartyBloc>(
            create: (context) => SharePartyBloc(),
            child: const ProfilePage(),
          ),
        ],
      ),
      bottomNavigationBar: ClipRRect(
        borderRadius: const BorderRadius.vertical(
          top: Radius.circular(24.0),
        ),
        child: NavigationBarTheme(
          data: NavigationBarThemeData(
            indicatorColor: Colors.blue.shade100,
          ),
          child: StreamBuilder<int>(
            stream: _selectedPage,
            builder: (context, selectedPage) {
              return NavigationBar(
                height: 60.0,
                selectedIndex: selectedPage.data!,
                backgroundColor: Colors.blue.shade50,
                animationDuration: const Duration(milliseconds: 300),
                onDestinationSelected: (selectedDestination) =>
                    _pageController.animateToPage(
                  selectedDestination,
                  duration: const Duration(milliseconds: 500),
                  curve: Curves.ease,
                ),
                destinations: <Widget>[
                  const NavigationDestination(
                    icon: Icon(Icons.library_books_outlined),
                    label: 'Cocktails',
                    selectedIcon: Icon(Icons.library_books),
                  ),
                  const NavigationDestination(
                    icon: Icon(Icons.add_box_outlined),
                    label: 'Create party',
                    selectedIcon: Icon(Icons.add_box),
                  ),
                  NavigationDestination(
                    icon: avatarUrl != null
                        ? CircleAvatar(
                            radius: 12.0,
                            backgroundImage: NetworkImage(avatarUrl!),
                          )
                        : const Icon(Icons.person_outlined),
                    label: 'Profile',
                  ),
                ],
              );
            },
          ),
        ),
      ),
    );
  }
}
