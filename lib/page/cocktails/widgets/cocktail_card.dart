import 'package:blendify/domain/entity/cocktail.dart';
import 'package:flutter/material.dart';

enum ButtonType { delete, like }

class CocktailCard extends StatelessWidget {
  const CocktailCard({
    Key? key,
    required this.cocktail,
    required this.buttonType,
    required this.onButtonPressed,
  }) : super(key: key);

  final Cocktail cocktail;
  final ButtonType buttonType;
  final VoidCallback onButtonPressed;

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 1.5,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(16.0),
      ),
      color: Colors.blue.shade50,
      shadowColor: Colors.black26,
      margin: const EdgeInsets.all(8.0),
      child: Padding(
        padding: const EdgeInsets.all(4.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Container(
              height: 180.0,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(16.0),
                image: DecorationImage(
                  fit: BoxFit.cover,
                  image: NetworkImage(cocktail.imageUrl),
                ),
              ),
            ),
            const SizedBox(height: 12.0),
            Text(
              cocktail.title,
              textAlign: TextAlign.center,
            ),
            const Spacer(),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 4.0),
              child: ElevatedButton(
                onPressed: onButtonPressed,
                child: Text(
                  buttonType == ButtonType.delete ? 'Delete' : 'Like',
                ),
                style: ElevatedButton.styleFrom(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(16.0),
                  ),
                  primary: buttonType == ButtonType.delete
                      ? Colors.red.shade400
                      : Colors.blue.shade400,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
