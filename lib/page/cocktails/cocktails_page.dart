import 'package:blendify/domain/bloc/cocktails/cocktails_bloc.dart';
import 'package:blendify/domain/entity/cocktail.dart';
import 'package:blendify/page/cocktails/widgets/cocktail_card.dart';
import 'package:blendify/page/widgets/loader.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/material.dart';

class CocktailsPage extends StatefulWidget {
  const CocktailsPage({Key? key}) : super(key: key);

  @override
  State<CocktailsPage> createState() => _CocktailsPageState();
}

class _CocktailsPageState extends State<CocktailsPage>
    with TickerProviderStateMixin {
  late TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 2, vsync: this);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    context.read<CocktailsBloc>()
      ..add(const LoadRandomCocktails())
      ..add(const LoadLikedCocktails());
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.blue.shade200,
        title: const Text('Cocktails'),
        centerTitle: true,
        bottom: TabBar(
          indicatorColor: Colors.white,
          controller: _tabController,
          tabs: const <Widget>[
            Tab(child: Text('Random')),
            Tab(child: Text('Liked')),
          ],
        ),
      ),
      floatingActionButton: SizedBox(
        height: 52.0,
        width: 200.0,
        child: ElevatedButton(
          onPressed: () => context.read<CocktailsBloc>().add(
                const LoadRandomCocktails(),
              ),
          child: const Text('Reload cocktails'),
          style: ElevatedButton.styleFrom(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(24.0),
            ),
          ),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      body: SafeArea(
        child: BlocBuilder<CocktailsBloc, CocktailsState>(
          builder: (context, state) {
            return Stack(
              children: <Widget>[
                TabBarView(
                  physics: const BouncingScrollPhysics(),
                  controller: _tabController,
                  children: <Widget>[
                    GridView.count(
                      crossAxisCount: 2,
                      childAspectRatio: 0.65,
                      children: state.randomCocktails
                          .map(
                            (cocktail) => CocktailCard(
                              cocktail: cocktail,
                              buttonType: ButtonType.like,
                              onButtonPressed: () =>
                                  _onLikeButtonPressed(cocktail),
                            ),
                          )
                          .toList(),
                    ),
                    GridView.count(
                      crossAxisCount: 2,
                      childAspectRatio: 0.65,
                      children: state.likedCocktails
                          .map(
                            (cocktail) => CocktailCard(
                              cocktail: cocktail,
                              buttonType: ButtonType.delete,
                              onButtonPressed: () =>
                                  _onDeleteButtonPressed(cocktail.id),
                            ),
                          )
                          .toList(),
                    ),
                  ],
                ),
                if (state.isLoading) const Loader(),
              ],
            );
          },
        ),
      ),
    );
  }

  void _onLikeButtonPressed(Cocktail cocktail) {
    context.read<CocktailsBloc>().add(
          LikeCocktailButtonPressed(cocktail: cocktail),
        );
  }

  void _onDeleteButtonPressed(String cocktailId) {
    context
        .read<CocktailsBloc>()
        .add(DeleteCocktailButtonPressed(cocktailId: cocktailId));
  }
}
