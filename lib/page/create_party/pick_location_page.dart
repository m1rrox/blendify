import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class PickLocationPage extends StatefulWidget {
  const PickLocationPage({Key? key}) : super(key: key);

  @override
  State<PickLocationPage> createState() => _PickLocationPageState();
}

class _PickLocationPageState extends State<PickLocationPage> {
  final _lvivLocation = const LatLng(49.842957, 24.031111);
  final CameraPosition _initialCameraPosition = const CameraPosition(
    target: LatLng(49.842957, 24.031111),
    zoom: 12.0,
  );

  late BehaviorSubject<LatLng> _partyLocation;
  late BehaviorSubject<Marker> _marker;

  @override
  void initState() {
    super.initState();
    _partyLocation = BehaviorSubject.seeded(_lvivLocation);
    _marker = BehaviorSubject.seeded(Marker(
      markerId: const MarkerId(''),
      position: _lvivLocation,
      icon: BitmapDescriptor.defaultMarkerWithHue(
        BitmapDescriptor.hueBlue,
      ),
    ));
  }

  @override
  void dispose() {
    _marker.close();
    _partyLocation.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          StreamBuilder<Marker>(
            stream: _marker,
            builder: (context, marker) {
              return GoogleMap(
                mapType: MapType.normal,
                initialCameraPosition: _initialCameraPosition,
                markers: <Marker>{marker.data!},
                onTap: (latLng) {
                  _partyLocation.add(latLng);
                  _marker.add(
                    Marker(
                      markerId: MarkerId(latLng.toString()),
                      position: latLng,
                      icon: BitmapDescriptor.defaultMarkerWithHue(
                        BitmapDescriptor.hueBlue,
                      ),
                    ),
                  );
                },
              );
            },
          ),
          Positioned(
            left: 16.0,
            right: 16.0,
            bottom: 48.0,
            child: ConstrainedBox(
              constraints: const BoxConstraints(
                minHeight: 48.0,
              ),
              child: ElevatedButton(
                onPressed: () => Navigator.pop(context, _partyLocation.value),
                child: const Text('Pick'),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
