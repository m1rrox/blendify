import 'package:blendify/domain/bloc/create_party/create_party_bloc.dart';
import 'package:blendify/domain/entity/party.dart';
import 'package:blendify/page/create_party/location_photo_page.dart';
import 'package:blendify/page/create_party/name_description_page.dart';
import 'package:blendify/page/create_party/select_date_page.dart';
import 'package:flow_builder/flow_builder.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/material.dart';

class CreatePartyPage extends StatefulWidget {
  const CreatePartyPage({Key? key}) : super(key: key);

  @override
  State<CreatePartyPage> createState() => _CreatePartyPageState();
}

class _CreatePartyPageState extends State<CreatePartyPage> {
  late FlowController<Party> _flowController;

  @override
  void initState() {
    super.initState();
    _flowController = FlowController(Party.empty());
  }

  @override
  void dispose() {
    super.dispose();
    _flowController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Create party'),
        centerTitle: true,
      ),
      body: FlowBuilder<Party>(
        controller: _flowController,
        onGeneratePages: (party, pages) => [
          MaterialPage(child: NameDescriptionPage()),
          if (party.name.isNotEmpty && party.description.isNotEmpty)
            const MaterialPage(child: SelectDatePage()),
          if (party.type != null)
            const MaterialPage(child: LocationPhotoPage()),
        ],
        onComplete: (party) {
          context
              .read<CreatePartyBloc>()
              .add(FinishButtonPressed(party: party));
        },
      ),
    );
  }
}
