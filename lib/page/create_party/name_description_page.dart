import 'package:blendify/domain/entity/party.dart';
import 'package:flow_builder/flow_builder.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/subjects.dart';

class NameDescriptionPage extends StatefulWidget {
  const NameDescriptionPage({Key? key}) : super(key: key);

  @override
  State<NameDescriptionPage> createState() => _NameDescriptionPageState();
}

class _NameDescriptionPageState extends State<NameDescriptionPage> {
  late TextEditingController _nameController;
  late TextEditingController _descriptionController;
  late BehaviorSubject<bool> _isButtonEnabled;

  @override
  void initState() {
    super.initState();
    _nameController = TextEditingController(text: '');
    _descriptionController = TextEditingController(text: '');
    _isButtonEnabled = BehaviorSubject.seeded(false);
  }

  @override
  void dispose() {
    _descriptionController.dispose();
    _nameController.dispose();
    super.dispose();
  }

  void _onNextButtonPressed() {
    context.flow<Party>().update((party) => party.copyWith(
          name: _nameController.text,
          description: _descriptionController.text,
        ));
  }

  String? _validateTextField(String? inputValue, int minLength) {
    if (inputValue == null) return inputValue;

    if (inputValue.isEmpty) return 'Field is required';

    if (inputValue.length < minLength) {
      return 'Field should have minimum $minLength characters';
    }

    return null;
  }

  bool _isNextButtonEnabled() {
    return _validateTextField(_nameController.text, 4) == null &&
        _validateTextField(_descriptionController.text, 20) == null;
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            children: <Widget>[
              const SizedBox(height: 20.0),
              const Text(
                'Create interesting name for your event',
              ),
              const SizedBox(height: 8.0),
              TextFormField(
                controller: _nameController,
                textAlign: TextAlign.center,
                autovalidateMode: AutovalidateMode.onUserInteraction,
                validator: (inputValue) => _validateTextField(inputValue, 4),
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Name',
                ),
                onChanged: (value) =>
                    _isButtonEnabled.add(_isNextButtonEnabled()),
              ),
              const SizedBox(height: 20.0),
              const Text(
                'Provide members of your event with concise description',
              ),
              const SizedBox(height: 8.0),
              TextFormField(
                maxLines: 5,
                controller: _descriptionController,
                autovalidateMode: AutovalidateMode.onUserInteraction,
                validator: (inputValue) => _validateTextField(inputValue, 20),
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Description',
                ),
                onChanged: (value) =>
                    _isButtonEnabled.add(_isNextButtonEnabled()),
              ),
            ],
          ),
        ),
        Positioned(
          left: 16.0,
          right: 16.0,
          bottom: 24.0,
          child: StreamBuilder<bool>(
            stream: _isButtonEnabled,
            builder: (context, isEnabled) {
              return ElevatedButton(
                onPressed: isEnabled.data! ? _onNextButtonPressed : null,
                child: const Text('Next'),
              );
            },
          ),
        ),
      ],
    );
  }
}
