import 'package:blendify/domain/entity/party.dart';
import 'package:flow_builder/flow_builder.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/subjects.dart';
import 'package:intl/intl.dart';

class SelectDatePage extends StatefulWidget {
  const SelectDatePage({Key? key}) : super(key: key);

  @override
  State<SelectDatePage> createState() => _SelectDatePageState();
}

class _SelectDatePageState extends State<SelectDatePage> {
  final DateFormat dateFormater = DateFormat.MMMd().add_jm();
  late BehaviorSubject<DateTime> _startsAt;
  late BehaviorSubject<PartyType> _partyType;

  @override
  void initState() {
    super.initState();
    _startsAt = BehaviorSubject.seeded(context.flow<Party>().state.startAt);
    _partyType = BehaviorSubject.seeded(PartyType.private);
  }

  @override
  void dispose() {
    _startsAt.close();
    _partyType.close();
    super.dispose();
  }

  void _onNextButtonPressed() {
    context.flow<Party>().update((party) => party.copyWith(
          startAt: _startsAt.value,
          type: _partyType.value,
        ));
  }

  void _showDatePicker(
    BuildContext context, {
    required Function(DateTime) onChanged,
    required DateTime initialDate,
  }) {
    showCupertinoModalPopup(
      context: context,
      builder: (_) => Container(
        height: 400,
        color: Colors.white,
        child: CupertinoDatePicker(
          initialDateTime: initialDate,
          onDateTimeChanged: onChanged,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              const SizedBox(height: 20.0),
              const Text(
                'Select party date',
                textAlign: TextAlign.center,
              ),
              const SizedBox(height: 8.0),
              StreamBuilder<DateTime>(
                stream: _startsAt,
                builder: (context, snapshot) {
                  return OutlinedButton(
                    onPressed: () => _showDatePicker(
                      context,
                      onChanged: (dateTime) => _startsAt.add(dateTime),
                      initialDate: snapshot.data!,
                    ),
                    child: Text(dateFormater.format(snapshot.data!)),
                  );
                },
              ),
              const SizedBox(height: 20.0),
              const Text(
                'Select type of your party',
                textAlign: TextAlign.center,
              ),
              const SizedBox(height: 8.0),
              StreamBuilder<PartyType>(
                stream: _partyType,
                builder: (context, partyType) {
                  return CupertinoSlidingSegmentedControl<PartyType>(
                    groupValue: partyType.data,
                    children: const <PartyType, Widget>{
                      PartyType.public: Text('Public'),
                      PartyType.private: Text('Private')
                    },
                    onValueChanged: (partyType) {
                      _partyType.add(partyType!);
                    },
                  );
                },
              ),
            ],
          ),
        ),
        Positioned(
          left: 16.0,
          right: 16.0,
          bottom: 24.0,
          child: Row(
            children: <Widget>[
              Expanded(
                child: OutlinedButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: const Text('Back'),
                ),
              ),
              const SizedBox(width: 8.0),
              Expanded(
                child: ElevatedButton(
                  onPressed: _onNextButtonPressed,
                  child: const Text('Next'),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
