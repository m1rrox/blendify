import 'package:blendify/domain/bloc/create_party/create_party_bloc.dart';
import 'package:blendify/domain/entity/party.dart';
import 'package:blendify/domain/entity/position.dart';
import 'package:blendify/page/create_party/pick_location_page.dart';
import 'package:blendify/page/widgets/loader.dart';
import 'package:flow_builder/flow_builder.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class LocationPhotoPage extends StatefulWidget {
  const LocationPhotoPage({Key? key}) : super(key: key);

  @override
  State<LocationPhotoPage> createState() => _LocationPhotoPageState();
}

class _LocationPhotoPageState extends State<LocationPhotoPage> {
  void _onSelectLocationButtonPressed() async {
    final LatLng location = await Navigator.of(
      context,
      rootNavigator: true,
    ).push(
      MaterialPageRoute(
        builder: (context) => const PickLocationPage(),
      ),
    );

    final position = Position(
      longitude: location.longitude,
      latitude: location.latitude,
    );

    context.read<CreatePartyBloc>().add(LocationRetrieved(position: position));
  }

  void _onFinishButtonPressed(String avatarUrl, String location) {
    context.flow<Party>().complete((party) => party.copyWith(
          imageUrl: avatarUrl,
          location: location,
        ));
    
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CreatePartyBloc, CreatePartyState>(
      builder: (context, state) {
        return Stack(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  const SizedBox(height: 20.0),
                  const Text(
                    'Add a photo of how your party would look like',
                    textAlign: TextAlign.center,
                  ),
                  const SizedBox(height: 8.0),
                  GestureDetector(
                    onTap: () => context
                        .read<CreatePartyBloc>()
                        .add(const AvatarButtonPressed()),
                    child: Container(
                      height: 270.0,
                      width: 135.0,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        color: Colors.blue.shade200,
                        borderRadius: BorderRadius.circular(16.0),
                        image: DecorationImage(
                          image: NetworkImage(state.avatarUrl),
                          fit: BoxFit.cover,
                        ),
                      ),
                      child: state.avatarUrl.isEmpty
                          ? const Icon(Icons.add_a_photo, size: 32.0)
                          : const SizedBox.shrink(),
                    ),
                  ),
                  const SizedBox(height: 20.0),
                  Text(
                    state.location,
                    textAlign: TextAlign.center,
                  ),
                  const SizedBox(height: 8.0),
                  ElevatedButton(
                    onPressed: _onSelectLocationButtonPressed,
                    child: const Text('Select Location'),
                  ),
                ],
              ),
            ),
            Positioned(
              left: 16.0,
              right: 16.0,
              bottom: 24.0,
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: OutlinedButton(
                      onPressed: () => Navigator.of(context).pop(),
                      child: const Text('Back'),
                    ),
                  ),
                  const SizedBox(width: 8.0),
                  Expanded(
                    child: ElevatedButton(
                      onPressed: state.avatarUrl.isNotEmpty &&
                              state.location.isNotEmpty
                          ? () => _onFinishButtonPressed(
                                state.avatarUrl,
                                state.location,
                              )
                          : null,
                      child: const Text('Finish'),
                    ),
                  ),
                ],
              ),
            ),
            if (state.isLoading) const Loader(),
          ],
        );
      },
    );
  }
}
