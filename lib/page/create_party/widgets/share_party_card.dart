import 'package:blendify/domain/entity/party.dart';
import 'package:flutter/material.dart';

class SharePartyCard extends StatelessWidget {
  const SharePartyCard({Key? key, required this.party}) : super(key: key);

  final Party party;

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 1.5,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(16.0),
      ),
      color: Colors.blue.shade50,
      shadowColor: Colors.black26,
      child: Padding(
        padding: const EdgeInsets.all(4.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Container(
              height: 180.0,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(16.0),
                image: DecorationImage(
                  fit: BoxFit.cover,
                  image: NetworkImage(party.imageUrl),
                ),
              ),
            ),
            const SizedBox(height: 12.0),
            Text(
              party.name,
              textAlign: TextAlign.center,
            ),
            const SizedBox(height: 12.0),
            Text(
              party.startAt.toUtc().toString(),
              textAlign: TextAlign.center,
            ),
            const SizedBox(height: 12.0),
            Text(
              party.location,
              textAlign: TextAlign.center,
            ),
            const SizedBox(height: 12.0),
          ],
        ),
      ),
    );
  }
}
