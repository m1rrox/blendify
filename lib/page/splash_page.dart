import 'package:flutter/material.dart';
import '../config/assets.dart';
import '../config/text_styles.dart';

class SplashPage extends StatelessWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset(
              BlendifyAsset.blendifyLogo,
              height: 220.0,
              width: 220.0,
            ),
            const SizedBox(height: 36.0),
            Text(
              'Blendify',
              style: BlendifyTextStyle.heading1,
            ),
          ],
        ),
      ),
    );
  }
}
