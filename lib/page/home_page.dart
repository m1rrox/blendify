import 'package:blendify/data/repository/cocktail_repository.dart';
import 'package:blendify/data/repository/party_repository.dart';
import 'package:blendify/domain/bloc/cocktails/cocktails_bloc.dart';
import 'package:blendify/domain/bloc/create_party/create_party_bloc.dart';
import 'package:blendify/domain/entity/profile.dart';
import 'package:blendify/domain/repository/cocktail_repository.dart';
import 'package:blendify/domain/repository/party_repository.dart';
import 'package:blendify/util/location_manager.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dio/dio.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../domain/bloc/auth/auth_cubit.dart';
import '../domain/bloc/profile/profile_bloc.dart';
import '../domain/repository/profile_repository.dart';

import 'app_root_page.dart';
import 'auth_page.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthCubit, AuthState>(
      builder: (context, state) {
        if (state is Authenticated) {
          return MultiProvider(
            providers: [
              Provider<Profile>.value(value: state.profile),
              Provider<ICocktailRepository>(
                create: (context) => CocktailRepository(
                  currentUserId: context.read<Profile>().uid,
                  firestore: context.read<FirebaseFirestore>(),
                  networkClient: context.read<Dio>(),
                ),
              ),
              Provider<IPartyRepository>(
                create: (context) => PartyRepository(
                  firestore: context.read<FirebaseFirestore>(),
                  currentUserId: context.read<Profile>().uid,
                ),
              ),
              BlocProvider<ProfileBloc>(
                create: (context) => ProfileBloc(
                  profileRepository: context.read<IProfileRepository>(),
                  partyRepository: context.read<IPartyRepository>(),
                ),
              ),
              BlocProvider<CreatePartyBloc>(
                create: (context) => CreatePartyBloc(
                  currentUserId: context.read<Profile>().uid,
                  partyRepository: context.read<IPartyRepository>(),
                  locationManager: context.read<LocationManager>(),
                  storage: context.read<FirebaseStorage>(),
                  imagePicker: context.read<ImagePicker>(),
                ),
              ),
              BlocProvider<CocktailsBloc>(
                create: (context) => CocktailsBloc(
                  cocktailRepository: context.read<ICocktailRepository>(),
                ),
              ),
            ],
            child: const AppRootPage(),
          );
        }

        return const AuthPage();
      },
    );
  }
}
