import 'package:blendify/util/location_manager.dart';
import 'package:dio/dio.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:image_picker/image_picker.dart';

import 'data/repository/profile_repository.dart';
import 'domain/repository/profile_repository.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import 'domain/bloc/auth/auth_cubit.dart';
import 'domain/bloc/init/init_bloc.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'page/home_page.dart';
import 'page/splash_page.dart';

class BlendifyApp extends StatefulWidget {
  const BlendifyApp({Key? key}) : super(key: key);

  @override
  State<BlendifyApp> createState() => _BlendifyAppState();
}

class _BlendifyAppState extends State<BlendifyApp> {
  @override
  void initState() {
    super.initState();
    context.read<InitBloc>().add(const AppStarted());
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: BlocBuilder<InitBloc, InitState>(
        builder: (context, state) {
          if (state is AppInitInProgress) {
            return const SplashPage();
          }

          return MultiProvider(
            providers: [
              Provider<FirebaseAuth>.value(
                value: FirebaseAuth.instance,
              ),
              Provider<FirebaseFirestore>.value(
                value: FirebaseFirestore.instance,
              ),
              Provider<FirebaseStorage>.value(
                value: FirebaseStorage.instance,
              ),
              Provider<IProfileRepository>(
                create: (context) => ProfileRepository(
                  firestore: context.read<FirebaseFirestore>(),
                ),
              ),
              Provider<Dio>(create: (_) => Dio()),
              Provider<ImagePicker>(
                create: (_) => ImagePicker(),
              ),
              Provider<LocationManager>(
                create: (_) => const LocationManager(),
              ),
              BlocProvider<AuthCubit>(
                create: (context) => AuthCubit(
                  firebaseAuth: context.read<FirebaseAuth>(),
                ),
              ),
            ],
            child: const HomePage(),
          );
        },
      ),
    );
  }
}
